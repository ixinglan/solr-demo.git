package com.example;

import com.example.bean.Book;
import com.example.service.BookService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class ApplicationTest {
    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(ApplicationTest.class, args);
        BookService bookService = app.getBean("bookService", BookService.class);
        Book book = new Book("99", "solr", "solr就是ElasticSearch", 911.5);
        bookService.addBook(book);
        List<Book> books = bookService.queryBooks("name:so*");
        System.out.println(books);
    }
}
