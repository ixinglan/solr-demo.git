package com.example.service;

import com.example.bean.Book;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service("bookService")
public class BookService {

    @Autowired
    private SolrClient solrClient;

    // 添加数据
    public void addBook(Book book) {
        try {
            solrClient.addBean(book);
            solrClient.commit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
    }

    public List<Book> queryBooks(String query) {
        SolrQuery solrQuery = new SolrQuery();
        // 设置默认搜索域
        solrQuery.set("df", "desc");
        solrQuery.setQuery(query);
        // 高亮显示设置
        solrQuery.setHighlight(true);
        // 添加高亮显示的域
        solrQuery.addHighlightField("name desc");
        solrQuery.setHighlightSimplePre("<font color='red'>");
        solrQuery.setHighlightSimplePost("</font>");
        try {
            QueryResponse queryResponse = solrClient.query(solrQuery);
            if (queryResponse == null) {
                return null;
            }
            // 获取高亮
            Map<String, Map<String, List<String>>> hilightMap = queryResponse.getHighlighting();
            // 获取书的列表
            /* 需要将 managed-schema 里以下域修改一下类型，String字段改成string,不然写入后，查询出来，getBean(class z)会转换异常
            <field name="desc" type="text_general"/>
            <field name="id" type="string" multiValued="false" indexed="true" required="true" stored="true"/>
            <field name="name" type="text_general"/>
            <field name="price" type="pdoubles"/>

            修改为：
            <field name="desc" type="string"/>
            <field name="id" type="string" multiValued="false" indexed="true" required="true" stored="true"/>
            <field name="name" type="string"/>
            <field name="price" type="pdouble"/>
             */
            List<Book> bookList = queryResponse.getBeans(Book.class);
            // 赋值高亮信息
            for (Book book : bookList) {
                List<String> list = hilightMap.get(book.getId()).get("desc");
                if (!CollectionUtils.isEmpty(list)) {
                    book.setDescription(list.get(0));
                }
                list = hilightMap.get(book.getId()).get("name");
                if (!CollectionUtils.isEmpty(list)) {
                    book.setName(list.get(0));
                }
            }
            return bookList;
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //查询优化：游标查询
    public void cursorQuery() throws Exception {
        //solr查询封装
        SolrQuery sq = new SolrQuery();
        sq.setRows(2);//设置游标一次读的数量
        sq.set("q", "*:*");//按条件检索
        sq.setSort("id", SolrQuery.ORDER.asc);//根据主键排序
        String cursorMark = CursorMarkParams.CURSOR_MARK_START;//游标初始化
        boolean done = false;
        while (!done) {
            sq.set(CursorMarkParams.CURSOR_MARK_PARAM, cursorMark);//变化游标条件
            QueryResponse rsp = solrClient.query(sq);//执行多次查询读取
            String nextCursorMark = rsp.getNextCursorMark();//获取下次游标
            // 做一些操作数据的事
            for (SolrDocument sd : rsp.getResults()) {
                System.out.println(sd.get("id"));
            }
            //如果两次游标一样，说明数据拉取完毕，可以结束循环了
            if (cursorMark.equals(nextCursorMark)) {
                done = true;
            }
            cursorMark = nextCursorMark;
        }
    }
}
