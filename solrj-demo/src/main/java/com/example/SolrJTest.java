package com.example;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class SolrJTest {
    private String serverUrl = "http://qiang1:8983/solr/qiang_core_1";

    @Test
    public void testSaveOrUpdateIndex() throws Exception {
        // 创建客户端
        HttpSolrClient client = new HttpSolrClient.Builder(serverUrl).build();
        SolrInputDocument doc = new SolrInputDocument();
        // 添加文档数据
        doc.addField("id", "43");
        doc.addField("name", "ES");
        doc.addField("desc", "Lucene Solr ES java架构师都得学 最重点的是ES");
        doc.addField("price", 800);
        client.add(doc);
        client.commit();
        client.close();
    }

    @Test
    public void testDelete() throws Exception {
        // 创建客户端
        HttpSolrClient client = new HttpSolrClient.Builder(serverUrl).build();
        // 根据id 删除
        client.deleteById("99");
        // 根据查询删除
//        client.deleteByQuery("name:ES");
        client.commit();
        client.close();
    }

    @Test
    public void testSimpleQuery() throws Exception {
        // 创建客户端
        HttpSolrClient client = new HttpSolrClient.Builder(serverUrl).build();
        // 创建查询对象
        SolrQuery query = new SolrQuery();
        // 设置查询条件
        query.set("q", "name:*");

        // 执行查询
        QueryResponse queryResponse = client.query(query);
        // 获取文档列表
        SolrDocumentList documentList = queryResponse.getResults();
        for (SolrDocument solrDocument : documentList) {
            System.out.println("id:" + solrDocument.get("id"));
            System.out.println("name:" + solrDocument.get("name"));
            System.out.println("desc:" + solrDocument.get("desc"));
            System.out.println("price:" + solrDocument.get("price"));
        }

        client.close();
    }

    @Test
    public void testComplexQuery() throws Exception {
        // 创建客户端
        HttpSolrClient client = new HttpSolrClient.Builder(serverUrl).build();
        SolrQuery query = new SolrQuery();
        query.setQuery("solr");
        // 过滤条件
        query.setFilterQueries("name:solr");
        // 排序
        query.setSort("price", SolrQuery.ORDER.desc);
        // 分页
        query.setStart(0);
        query.setRows(5);
        // 设置返回的域
        query.setFields("id", "name", "price", "desc");
        // 设置默认搜索域
        query.set("df", "desc");
        // 高亮设置
        query.setHighlight(true);
        query.addHighlightField("name desc");
        query.setHighlightSimplePre("<font color='red'>");
        query.setHighlightSimplePost("</font>");
        // 执行查询
        QueryResponse queryResponse = client.query(query);
        SolrDocumentList solrDocumentList = queryResponse.getResults();
        // 打印查询数据量
        System.out.println("查询到的书的数量:" + solrDocumentList.getNumFound());
        // 获取高亮信息
        Map<String, Map<String, List<String>>> bookHL = queryResponse.getHighlighting();
        // 遍历文档数据  显示高亮信息和普通信息
        for (SolrDocument solrDocument : solrDocumentList) {
            // 获取name 对应的 高亮信息
            String nameHL = "";
            List<String> list = bookHL.get(solrDocument.get("id")).get("name");
            // 判断是否有高亮内容
            if (null != list) {
                nameHL = list.get(0);
                System.out.println("nameHL=" + nameHL);
            }

            String descHL = "";
            List<String> list2 = bookHL.get(solrDocument.get("id")).get("desc");
            // 判断是否有高亮内容
            if (null != list2) {
                descHL = list2.get(0);
                System.out.println("descHL=" + descHL);
            }
            System.out.println("id:" + solrDocument.get("id"));
            System.out.println("name:" + solrDocument.get("name"));
            System.out.println("desc:" + solrDocument.get("desc"));
            System.out.println("price:" + solrDocument.get("price"));
        }
        client.close();
    }

}
